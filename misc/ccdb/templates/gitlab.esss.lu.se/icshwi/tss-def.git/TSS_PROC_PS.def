# Name: TSS_PROC_PS
# Description: Pressure Switch model for TSS
# Author: Evan Foy 
# Date: 07/04/2021
# Version: v1.0    First version
#v1.1    Renamed variables
#v1.2    Added Archive as default.
#    Added Validity PV (set in CCDB).
#v1.3    [PLCF#ValidityPV] is default (i.e. "Channel A/B/C" RIO device). Properties acquired from Safety PLC are linked to the Safety PLC IO device, for all slot instances.
#


############################
#  DEFAULTS
############################ 
set_defaults(ARCHIVE=True)
set_defaults(VALIDITY_PV="[PLCF#ValidityPV]")

############################
#  EXTERNAL VALIDITY PVS
############################ 
external_validity_pv("[PLCF#ValidityPV]",True)
external_validity_pv("Tgt-TSS1080:Ctrl-PLC-02:IO_dev_ok_SPLC",True)


############################
#  STATUS BLOCK
############################ 
define_status_block()

# Boolean properties
add_digital("RT_TestSw_Blkd",  PV_DESC ="Relay Train - Test Switch Blocked",  PV_ONAM="True",  PV_ZNAM="False")
add_digital("RT_TestSw_Frcd",  PV_DESC ="Relay Train - Test Switch Forced",  PV_ONAM="True",  PV_ZNAM="False")
add_digital("RT_TestSw_Norm",  PV_DESC ="Relay Train - Test Switch Normal",  PV_ONAM="True",  PV_ZNAM="False")
add_digital("PT_TestSw_Blkd",  PV_DESC ="PLC Train - Test Switch Blocked",  PV_ONAM="True",  PV_ZNAM="False")
add_digital("PT_TestSw_Frcd",  PV_DESC ="PLC Train - Test Switch Forced",  PV_ONAM="True",  PV_ZNAM="False")
add_digital("PT_TestSw_Norm",  PV_DESC ="PLC Train - Test Switch Normal",  PV_ONAM="True",  PV_ZNAM="False")

# Minor alarms
add_minor_alarm("RT_PressureOK", "Relay Train - Press NOK",  ALARM_IF=False,  PV_ONAM="Relay Train - Press OK")
add_minor_alarm("PT_PressureOK", "PLC Train - Press NOK",  ALARM_IF=False,  PV_ONAM="PLC Train - Press OK",  VALIDITY_PV="Tgt-TSS1080:Ctrl-PLC-02:IO_dev_ok_SPLC")

