# Startup for Tgt-TSS1080:Ctrl-IOC-01

# Load standard IOC startup scripts
require essioc
iocshLoad("$(essioc_DIR)/common_config.iocsh")

# Load PLC specific startup script
iocshLoad("iocsh/tgt_tss1080_ctrl_plc_02.iocsh")
